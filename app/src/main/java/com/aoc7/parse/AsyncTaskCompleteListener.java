package com.aoc7.parse;

/**
 * @author Elluminati elluminati.in
 */
public interface AsyncTaskCompleteListener {
	void onTaskCompleted(String response, int serviceCode);
}
