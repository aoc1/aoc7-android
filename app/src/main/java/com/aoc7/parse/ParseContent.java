package com.aoc7.parse;

import android.app.Activity;
import android.text.TextUtils;

import com.aoc7.models.AmbulanceDetail;
import com.aoc7.models.MedicalEquipment;
import com.aoc7.models.Staff;
import com.aoc7.utils.Const;
import com.aoc7.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class ParseContent {

    private Activity activity;
    private final String AMBULANCE_ID = "ambulance_id";
    private final String AMBULANCE_TYPE = "ambulance_type";
    private final String TRACKING_SPEED = "tracking_speed";
    private final String CURRENT_LAT = "tracking_latitude";
    private final String CURRENT_LNG = "tracking_longitude";
    private final String AMBULANCE_NO = "ambulance_no";
    private final String DEST_LAT = "ambulance_to_latitude";
    private final String DEST_LNG = "ambulance_to_longitude";
    private final String IMAGE_1 = "images_name_1";
    private final String IMAGE_2 = "images_name_2";
    private final String IS_ONLINE = "is_online";
    private final String AMBULANCE_NAME = "ambulance_name";
    private final String DRIVER_NAME = "driver_name";
    private final String MAP_IMAGE = "direction_map";
    private final String SRC_LAT = "ambulance_from_latitude";
    private final String SRC_LNG = "ambulance_from_longitude";
    private final String TRACKING_TIME = "tracking_add_time";
    private final String AMBULANCES = "ambulances";
    private final String MEDICAL_EQUIPMENT = "medical_equipment";
    private final String ICON_IMAGE = "icon_image";
    private final String NAME = "name";
    private final String STATE = "state";

    public ParseContent(Activity activity){
        this.activity = activity;
    }

    public boolean isSuccess(String response){
        final String IS_SUCCESS = "success";
        if(TextUtils.isEmpty(response))
            return false;

        try{
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getBoolean(IS_SUCCESS))
                return true;

        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public void parseUser(String response){
        final String PICTURE = "picture", FIRST_NAME = "first_name", LAST_NAME = "last_name",
                HOSPITAL_NAME = "hospital", TYPE = "login_type", VOIP_NO = "voip_no", GENDER = "gender";

        PreferenceHelper preferenceHelper = new PreferenceHelper(activity);
        try {
            JSONObject jsonObject = new JSONObject(response);
            preferenceHelper.putUserId(jsonObject.getInt(Const.Params.ID));
            preferenceHelper.putUserEmail(jsonObject.getString(Const.Params.EMAIL));
            preferenceHelper.putSessionToken(jsonObject.getString(Const.Params.TOKEN));
            preferenceHelper.putUserPicture(jsonObject.getString(PICTURE));
            preferenceHelper.putFirstName(jsonObject.getString(FIRST_NAME));
            preferenceHelper.putLastName(jsonObject.getString(LAST_NAME));
            preferenceHelper.putHospitalName(jsonObject.getString(HOSPITAL_NAME));
            preferenceHelper.putUserType(Integer.parseInt(jsonObject.getString(TYPE)));
            preferenceHelper.putVoipNo(jsonObject.getString(VOIP_NO));
            preferenceHelper.putGender(jsonObject.getString(GENDER));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void parseAmbulanceList(String response, ArrayList<AmbulanceDetail> ambulanceDetailList){
        final String HOSPITAL_NAME = "hospital_name";
        final String AMBULANCE_PICTURE = "ambulance_picture", STATE_ID = "state_id", IS_DB_ONLINE = "is_db_online";
        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONArray ambulanceArray = jsonObject.getJSONArray(AMBULANCES);
            if(ambulanceArray.length() > 0){
                for(int i=0; i<ambulanceArray.length(); i++){
                    JSONObject jObject = ambulanceArray.getJSONObject(i);
                    AmbulanceDetail ambDetail = new AmbulanceDetail();
                    ambDetail.setAmbulanceId(jObject.getInt(AMBULANCE_ID));
                    ambDetail.setType(jObject.getString(AMBULANCE_TYPE));
                    ambDetail.setIsOnline(jObject.getString(IS_ONLINE));
                    ambDetail.setIsDbOnline(jObject.getString(IS_DB_ONLINE));
                    ambDetail.setCurrentLat(Double.parseDouble(jObject.getString(CURRENT_LAT)));
                    ambDetail.setCurrentLng(Double.parseDouble(jObject.getString(CURRENT_LNG)));
                    ambDetail.setDestLat(Double.parseDouble(jObject.getString(DEST_LAT)));
                    ambDetail.setDestLng(Double.parseDouble(jObject.getString(DEST_LNG)));
                    ambDetail.setAmbulanceNo(URLDecoder.decode(jObject.getString(AMBULANCE_NO), "utf-8"));
                    ambDetail.setAmbPicture(jObject.getString(AMBULANCE_PICTURE));
                    ambDetail.setImages(jObject.getString(IMAGE_1));
                    ambDetail.setImages(jObject.getString(IMAGE_2));
                    ambDetail.setImages(jObject.getString(MAP_IMAGE));
                    ambDetail.setSpeed(jObject.getDouble(TRACKING_SPEED));
                    ambDetail.setName(URLDecoder.decode(jObject.getString(AMBULANCE_NAME), "utf-8"));
                    ambDetail.setDriverName(URLDecoder.decode(jObject.getString(DRIVER_NAME), "utf-8"));
                    ambDetail.setTrackingTime(jObject.getString(TRACKING_TIME));
                    ambDetail.setState(jObject.getString(STATE));
                    ambDetail.setStateId(jObject.getInt(STATE_ID));
                    ambDetail.setSrcLat(Double.parseDouble(jObject.getString(SRC_LAT)));
                    ambDetail.setSrcLng(Double.parseDouble(jObject.getString(SRC_LNG)));
                    ambDetail.setHospitalName(URLDecoder.decode(jObject.optString(HOSPITAL_NAME),"utf-8"));
                    JSONArray equipmentArray = jObject.getJSONArray(MEDICAL_EQUIPMENT);
                    if(equipmentArray.length() > 0){
                        for(int j=0; j<equipmentArray.length(); j++){
                            JSONObject object = equipmentArray.getJSONObject(j);
                            MedicalEquipment equipment = new MedicalEquipment();
                            equipment.setEquipmentId(object.getInt(Const.Params.ID));
                            equipment.setEquipmentName(object.getString(NAME));
                            equipment.setEquipmentIcon(object.getString(ICON_IMAGE));
                            ambDetail.setEquipmentList(equipment);
                        }
                    }
                    ambulanceDetailList.add(ambDetail);
                }
            }
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void parseNotificationList(String response, ArrayList<AmbulanceDetail> ambDetailList){
        String DATA = "data";
        String NOTIFICATION_TIME = "notification_add_time";
        try{
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray(DATA);
            if(jsonArray.length() > 0){
                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject jObject = jsonArray.getJSONObject(i);
                    AmbulanceDetail ambDetail = new AmbulanceDetail();
                    ambDetail.setAmbulanceId(jObject.getInt(AMBULANCE_ID));
                    ambDetail.setType(jObject.getString(AMBULANCE_TYPE));
                    ambDetail.setIsOnline(jObject.getString(IS_ONLINE));
                    ambDetail.setDestLat(Double.parseDouble(jObject.getString(DEST_LAT)));
                    ambDetail.setDestLng(Double.parseDouble(jObject.getString(DEST_LNG)));
                    ambDetail.setAmbulanceNo(URLDecoder.decode(jObject.getString(AMBULANCE_NO),"utf-8"));
                    ambDetail.setImages(jObject.getString(IMAGE_1));
                    ambDetail.setImages(jObject.getString(IMAGE_2));
                    ambDetail.setImages(jObject.getString(MAP_IMAGE));
                    ambDetail.setName(URLDecoder.decode(jObject.getString(AMBULANCE_NAME),"utf-8"));
                    ambDetail.setDriverName(jObject.getString(DRIVER_NAME));
                    ambDetail.setCurrentLat(Double.parseDouble(jObject.getString(CURRENT_LAT)));
                    ambDetail.setCurrentLng(Double.parseDouble(jObject.getString(CURRENT_LNG)));
                    ambDetail.setSrcLat(Double.parseDouble(jObject.optString(SRC_LAT)));
                    ambDetail.setSrcLng(Double.parseDouble(jObject.optString(SRC_LNG)));
                    ambDetail.setTrackingTime(jObject.getString(NOTIFICATION_TIME));
                    ambDetailList.add(ambDetail);
                }
            }
        }
        catch(JSONException | UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }

    public AmbulanceDetail parseAmbulanceImage(String response){
        final String NUMBER_LIST = "number_list";
        final String NUMBER = "number", EMS_STAFF = "ems_staff", ER_STAFF = "er_staff",
                DOCTOR = "doctor", NURSE = "nurse", DRIVER = "driver", ER = "er", EMS = "ems", DISEASE_NAME = "patient_sick",
                DEPARTURE_TIME = "request_add_time", SOURCE_HOSPITAL = "source_hospital", DEST_HOSPITAL = "dest_hospital", FAST_TRACK = "fast_track";

        AmbulanceDetail ambDetail = new AmbulanceDetail();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray ambulanceArray = jsonObject.getJSONArray(AMBULANCES);
            if(ambulanceArray.length() > 0){
                JSONObject jObject = ambulanceArray.getJSONObject(0);
                ambDetail.setImages(jObject.getString(IMAGE_1));
                ambDetail.setImages(jObject.getString(IMAGE_2));
                ambDetail.setImages(jObject.getString(MAP_IMAGE));
                ambDetail.setSpeed(jObject.getDouble(TRACKING_SPEED));
                ambDetail.setTrackingTime(jObject.getString(TRACKING_TIME));
                ambDetail.setState(jObject.getString(STATE));
                ambDetail.setAmbulanceId(jObject.getInt(AMBULANCE_ID));
                ambDetail.setType(jObject.getString(AMBULANCE_TYPE));
                ambDetail.setIsOnline(jObject.getString(IS_ONLINE));
                ambDetail.setCurrentLat(Double.parseDouble(jObject.getString(CURRENT_LAT)));
                ambDetail.setCurrentLng(Double.parseDouble(jObject.getString(CURRENT_LNG)));
                ambDetail.setDestLat(Double.parseDouble(jObject.getString(DEST_LAT)));
                ambDetail.setDestLng(Double.parseDouble(jObject.getString(DEST_LNG)));
                ambDetail.setAmbulanceNo(URLDecoder.decode(jObject.getString(AMBULANCE_NO), "utf-8"));
                ambDetail.setName(URLDecoder.decode(jObject.getString(AMBULANCE_NAME), "utf-8"));
                ambDetail.setDriverName(URLDecoder.decode(jObject.getString(DRIVER_NAME), "utf-8"));
                ambDetail.setDoctor(jObject.getInt(DOCTOR));
                ambDetail.setDriver(jObject.getInt(DRIVER));
                ambDetail.setNurse(jObject.getInt(NURSE));
                ambDetail.setEr(jObject.getInt(ER));
                ambDetail.setEms(jObject.getInt(EMS));
                ambDetail.setDepartureTime(jObject.getString(DEPARTURE_TIME));
                ambDetail.setDiseaseName(jObject.getString(DISEASE_NAME));
                ambDetail.setFastTrack(jObject.getString(FAST_TRACK));
                JSONArray equipmentArray = jObject.getJSONArray(MEDICAL_EQUIPMENT);
                if (equipmentArray.length() > 0) {
                    for (int j = 0; j < equipmentArray.length(); j++) {
                        JSONObject object = equipmentArray.getJSONObject(j);
                        MedicalEquipment equipment = new MedicalEquipment();
                        equipment.setEquipmentId(object.getInt(Const.Params.ID));
                        equipment.setEquipmentName(object.getString(NAME));
                        equipment.setEquipmentIcon(object.getString(ICON_IMAGE));
                        ambDetail.setEquipmentList(equipment);
                    }
                }
                if (jObject.has(NUMBER_LIST)) {
                    JSONArray numberArray = jObject.getJSONArray(NUMBER_LIST);
                    if (numberArray.length() > 0) {
                        ArrayList<Staff> staffList = new ArrayList<>();
                        for (int j = 0; j < numberArray.length(); j++) {
                            JSONObject object = numberArray.getJSONObject(j);
                            if (object.has(DOCTOR)) {
                                JSONArray staffArray = object.getJSONArray(DOCTOR);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setDesignation(DOCTOR);
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }
                            if (object.has(NURSE)) {
                                JSONArray staffArray = object.getJSONArray(NURSE);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setDesignation(NURSE);
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }
                            if (object.has(DRIVER)) {
                                JSONArray staffArray = object.getJSONArray(DRIVER);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setDesignation(DRIVER);
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }
                            if (object.has(EMS_STAFF)) {
                                JSONArray staffArray = object.getJSONArray(EMS_STAFF);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setDesignation(EMS_STAFF);
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }
                            if (object.has(ER_STAFF)) {
                                JSONArray staffArray = object.getJSONArray(ER_STAFF);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setDesignation(ER_STAFF);
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }

                            if (object.has(SOURCE_HOSPITAL)) {
                                JSONArray staffArray = object.getJSONArray(SOURCE_HOSPITAL);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            ambDetail.setHospitalList(jsonObject1.getString(NAME));
                                        }
                                    }
                                }
                            }

                            if (object.has(DEST_HOSPITAL)) {
                                JSONArray staffArray = object.getJSONArray(DEST_HOSPITAL);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if(!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            ambDetail.setHospitalList(jsonObject1.getString(NAME));
                                        }
                                    }
                                }
                            }
                        }

                        ambDetail.setStaffList(staffList);
                    }
                }
                ambDetail.setSrcLat(Double.parseDouble(jObject.getString(SRC_LAT)));
                ambDetail.setSrcLng(Double.parseDouble(jObject.getString(SRC_LNG)));
            }
        }
        catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ambDetail;
    }
}
