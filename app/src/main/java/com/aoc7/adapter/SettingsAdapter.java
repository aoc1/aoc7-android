package com.aoc7.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.aoc7.R;

import java.util.ArrayList;

/**
 * Created by mypc on 21/6/16.
 */
public class SettingsAdapter extends BaseAdapter{

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Integer> images;

    public SettingsAdapter(Context context){
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Integer getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = inflater.inflate(R.layout.adapter_settings, parent, false);
            holder.ivSettingIcon = (ImageView) convertView.findViewById(R.id.ivSettingIcon);
            convertView.setTag(holder);
        }
        else{
            holder = (Holder) convertView.getTag();
        }

        switch (position){
            case 0:
                holder.ivSettingIcon.setImageResource(R.mipmap.profile_icon);
                break;

            case 1:
                holder.ivSettingIcon.setImageResource(R.mipmap.alert_icon);
                break;

            case 2:
                holder.ivSettingIcon.setImageResource(R.mipmap.report_icon);
                break;

            case 3:
                holder.ivSettingIcon.setImageResource(R.mipmap.setting);
                break;

            case 4:
                holder.ivSettingIcon.setImageResource(R.mipmap.feedback_icon);
                break;

            case 5:
                holder.ivSettingIcon.setImageResource(R.mipmap.info_icon);
                break;
        }

        return convertView;
    }

    private class Holder{
        ImageView ivSettingIcon;
    }
}
