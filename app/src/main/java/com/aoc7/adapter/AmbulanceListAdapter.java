package com.aoc7.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.aoc7.BaseActivity;
import com.aoc7.R;
import com.aoc7.models.AmbulanceDetail;
import com.aoc7.utils.Const;
import com.hb.views.PinnedSectionListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import de.hdodenhof.circleimageview.CircleImageView;

public class AmbulanceListAdapter extends BaseAdapter implements PinnedSectionListView.PinnedSectionListAdapter, Filterable, View.OnClickListener{
    private Context context;
    private ArrayList<AmbulanceDetail> ambulanceDetailList, filteredAmbList = new ArrayList<>(), filteredDetailList;
    private LayoutInflater inflater;
    private TreeSet<Integer> mSeparatorSet;
    private final int TYPE_ITEM = 0;
    private final int TYPE_SEPARATOR = 1;
    public boolean isFilterByName;
    private ImageOptions imageOptions;

    public AmbulanceListAdapter(Context context, TreeSet<Integer> mSeparatorSet, ArrayList<AmbulanceDetail> ambulanceDetailList){
        this.context = context;
        this.ambulanceDetailList = new ArrayList<>(ambulanceDetailList);
        this.filteredDetailList = ambulanceDetailList;
        this.mSeparatorSet = mSeparatorSet;
        inflater = LayoutInflater.from(context);
        imageOptions = new ImageOptions();
        imageOptions.fileCache = true;
        imageOptions.memCache = true;
        imageOptions.fallback = R.mipmap.ic_launcher;
    }

    @Override
    public int getCount() {
        return filteredDetailList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredDetailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_SEPARATOR + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if(mSeparatorSet.contains(position))
            return TYPE_SEPARATOR;
        else
            return TYPE_ITEM;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        SectionHolder sectionHolder = null;
        int type = getItemViewType(position);
        if(convertView == null){
            switch (type) {
                case TYPE_ITEM:
                    holder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.adapter_ambulance_list_item, parent, false);
                    holder.ivAmbulanceImg = (CircleImageView) convertView.findViewById(R.id.ivAmbulanceImg);
                    holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
                    holder.tvEta = (TextView) convertView.findViewById(R.id.tvEta);
                    holder.tvType = (TextView) convertView.findViewById(R.id.tvType);
                    holder.ivCamera = (ImageView) convertView.findViewById(R.id.ivCamera);
                    holder.ivStatus = (ImageView) convertView.findViewById(R.id.ivStatus);
//                    holder.llIcons = (LinearLayout) convertView.findViewById(R.id.llIcons);
                    holder.ivCamera.setOnClickListener(this);
                    holder.ivStatus.setOnClickListener(this);
                    convertView.setTag(holder);
                    break;

                case TYPE_SEPARATOR:
                    sectionHolder = new SectionHolder();
                    convertView = inflater.inflate(R.layout.adapter_ambulance_list_header, parent, false);
                    sectionHolder.tvHospitalName = (TextView) convertView.findViewById(R.id.tvHospitalName);
                    convertView.setTag(sectionHolder);
                    break;
            }
        }
        else{
            switch (type) {
                case TYPE_ITEM:
                    holder = (ViewHolder) convertView.getTag();
                    break;

                case TYPE_SEPARATOR:
                    sectionHolder = (SectionHolder) convertView.getTag();
            }
        }

        switch (type) {
            case TYPE_ITEM:
                AmbulanceDetail detail = filteredDetailList.get(position);
                new AQuery(convertView).id(holder.ivAmbulanceImg).image(detail.getAmbPicture(), imageOptions);
                holder.tvTitle.setText(detail.getName());
                holder.tvType.setText(detail.getType());
                holder.ivStatus.setTag(position);
                holder.ivCamera.setTag(position);

                if (filteredDetailList.get(position).getIsOnline().equalsIgnoreCase("yes")) {

                    holder.ivCamera.setVisibility(View.VISIBLE);
                    holder.tvEta.setVisibility(View.VISIBLE);
                    holder.tvEta.setText(context.getResources().getString(R.string.text_eta) + ": " + detail.getEta());

                    if ((detail.getSpeed() * 60 * 60 / 1000) > 80 && (detail.getSpeed() * 60 * 60 / 1000) < 120) {
                        holder.tvEta.setTextColor(ContextCompat.getColor(context, R.color.color_orange));
                    } else if ((detail.getSpeed() * 60 * 60 / 1000) >= 120) {
                        holder.tvEta.setTextColor(ContextCompat.getColor(context, R.color.color_red));
                    } else {
                        holder.tvEta.setTextColor(ContextCompat.getColor(context, R.color.color_green));
                    }

                } else {
                    holder.ivCamera.setVisibility(View.GONE);
                    holder.tvEta.setVisibility(View.GONE);
                }

                if(Const.isMobile) {
                switch (detail.getStateId()) {
                    case 0:
                        holder.ivStatus.setImageResource(R.mipmap.operation_assigned);
                        break;

                    case 1:
                        holder.ivStatus.setImageResource(R.mipmap.start_operation);
                        break;

                    case 2:
                        holder.ivStatus.setImageResource(R.mipmap.depart_from_hospital_base);
                        break;

                    case 3:
                        if(detail.getType().contains(Const.TYPE_EMS))
                            holder.ivStatus.setImageResource(R.mipmap.arrival_at_scene);
                        else
                            holder.ivStatus.setImageResource(R.mipmap.arrival_destination_hospital);
                        break;

                    case 4:
                        if(detail.getType().contains(Const.TYPE_EMS))
                            holder.ivStatus.setImageResource(R.mipmap.depart_from_scene);
                        else
                            holder.ivStatus.setImageResource(R.mipmap.start_comming_back_to_hospital_base);
                        break;

                    case 5:
                        if(detail.getType().contains(Const.TYPE_EMS))
                            holder.ivStatus.setImageResource(R.mipmap.arrival_destination_hospital);
                        else
                            holder.ivStatus.setImageResource(R.mipmap.arrival_hospital_base);
                        break;

                    case 6:
                        holder.ivStatus.setImageResource(R.mipmap.start_comming_back_to_hospital_base);
                        break;

                    case 7:
                        holder.ivStatus.setImageResource(R.mipmap.arrival_hospital_base);
                        break;
                }
            }
            else{
                switch (detail.getStateId()) {
                    case 0:
                        holder.ivStatus.setImageResource(R.mipmap.operation_assigned_tab);
                        break;

                    case 1:
                        holder.ivStatus.setImageResource(R.mipmap.start_operation_tab);
                        break;

                    case 2:
                        holder.ivStatus.setImageResource(R.mipmap.depart_from_hospital_base_tab);
                        break;

                    case 3:
                        if(detail.getType().contains(Const.TYPE_EMS))
                            holder.ivStatus.setImageResource(R.mipmap.arrival_at_scene_tab);
                        else
                            holder.ivStatus.setImageResource(R.mipmap.arrival_destination_hospital_tab);
                        break;

                    case 4:
                        if(detail.getType().contains(Const.TYPE_EMS))
                            holder.ivStatus.setImageResource(R.mipmap.depart_from_scene_tab);
                        else
                            holder.ivStatus.setImageResource(R.mipmap.start_comming_back_to_hospital_base_tab);
                        break;

                    case 5:
                        if(detail.getType().contains(Const.TYPE_EMS))
                            holder.ivStatus.setImageResource(R.mipmap.arrival_destination_hospital_tab);
                        else
                            holder.ivStatus.setImageResource(R.mipmap.arrival_hospital_base_tab);
                        break;

                    case 6:
                        holder.ivStatus.setImageResource(R.mipmap.start_comming_back_to_hospital_base_tab);
                        break;

                    case 7:
                        holder.ivStatus.setImageResource(R.mipmap.arrival_hospital_base_tab);
                        break;
                }
            }

            break;

            case TYPE_SEPARATOR:
                if(filteredDetailList.get(position).getHospitalName() != null) {
                    sectionHolder.tvHospitalName.setText(filteredDetailList.get(position).getHospitalName());
                }
                break;
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                filteredAmbList.clear();
                if(isFilterByName)
                    filteredAmbList = filterByName(constraint.toString());
                else
                    filteredAmbList = filterByType(constraint.toString());

                mSeparatorSet = new TreeSet<>();
                ArrayList<AmbulanceDetail> resultListOrg = new ArrayList<>();
                ArrayList<String> hospitalList = new ArrayList<>();

                HashSet<String> listToSet = new HashSet<>();
                for(int i=0; i<filteredAmbList.size(); i++){
                    if(listToSet.add(filteredAmbList.get(i).getHospitalName())){
                        hospitalList.add(filteredAmbList.get(i).getHospitalName());
                    }
                }

                for(int i=0; i<hospitalList.size(); i++){
                    AmbulanceDetail ambulanceDetail = new AmbulanceDetail();
                    ambulanceDetail.setHospitalName(hospitalList.get(i));

                    resultListOrg.add(ambulanceDetail);
                    mSeparatorSet.add(resultListOrg.size() - 1);
                    for(int j=0; j<filteredAmbList.size(); j++){
                        if(ambulanceDetail.getHospitalName().equals(filteredAmbList.get(j).getHospitalName())){
                            resultListOrg.add(filteredAmbList.get(j));
                        }
                    }
                }

                results.values = resultListOrg;
                results.count = resultListOrg.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    filteredDetailList.clear();
                    filteredDetailList = (ArrayList<AmbulanceDetail>) results.values;
                    notifyDataSetChanged();
                }
                else{
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    private ArrayList<AmbulanceDetail> filterByName(String constraint){
        for(int i=0; i<ambulanceDetailList.size(); i++){
            if(ambulanceDetailList.get(i).getName().contains(constraint)){
                filteredAmbList.add(ambulanceDetailList.get(i));
            }
        }

        return filteredAmbList;
    }

    private ArrayList<AmbulanceDetail> filterByType(String type){
        filteredDetailList.clear();
        Log.i("ambAdapter","selectedType--->>" + type);
        if(type.equalsIgnoreCase(Const.TYPE_ALL)){
            for(int i=0; i<ambulanceDetailList.size(); i++){
                if(ambulanceDetailList.get(i).getType() != null) {
                    filteredDetailList.add(ambulanceDetailList.get(i));
                }
            }
        }
        else if(type.equalsIgnoreCase(context.getString(R.string.text_stb))){
            for (int i = 0; i < ambulanceDetailList.size(); i++) {
                if(ambulanceDetailList.get(i).getIsOnline() != null) {
                    if (ambulanceDetailList.get(i).getIsOnline().equalsIgnoreCase("NO")) {
                        filteredDetailList.add(ambulanceDetailList.get(i));
                    }
                }
            }
        }
        else {
            for (int i = 0; i < ambulanceDetailList.size(); i++) {
                if(ambulanceDetailList.get(i).getType() != null) {
                    Log.i("ambAdapter", "type--->>" + ambulanceDetailList.get(i).getType());
                    if (ambulanceDetailList.get(i).getType().contains(type)) {
                        Log.i("ambAdapter", "filter by type--->> if called");
                        filteredDetailList.add(ambulanceDetailList.get(i));
                    }
                }
            }
        }
        return filteredDetailList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivCamera:
            case R.id.ivStatus:
                ImageView iv = (ImageView) v;
                ((BaseActivity)context).ambulanceDetail = ambulanceDetailList.get((int)iv.getTag());
                ((BaseActivity)context).isFromBottomBar = false;
//                ((BaseActivity)context).isNotification = false;
                ((BaseActivity)context).loadAmbulanceImagesFragment();
                break;
        }
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == TYPE_SEPARATOR;
    }

    class ViewHolder{
        CircleImageView ivAmbulanceImg;
        TextView tvTitle, tvEta, tvType;
        ImageView ivStatus, ivCamera;
//        LinearLayout llIcons;
    }

    class SectionHolder{
        TextView tvHospitalName;
    }
}
