package com.aoc7;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aoc7.fragments.AmbulanceImagesFragment;
import com.aoc7.fragments.MapFragment;
import com.aoc7.utils.Const;

public class MapActivity extends BaseActivity {

    private MapFragment mapFragment;
    public FrameLayout frameLayout;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        frameLayout = (FrameLayout) findViewById(R.id.contentFrame);
        llBottom = (LinearLayout) findViewById(R.id.llBottom);
        assert llBottom != null;
        ivAmbulance = (ImageView) llBottom.findViewById(R.id.ivAmbulance);
        ivAmbulance.setOnClickListener(this);
        ivMap = (ImageView) llBottom.findViewById(R.id.ivMap);
        ivMap.setOnClickListener(this);
        ivNotification = (ImageView) llBottom.findViewById(R.id.ivNotification);
        ivNotification.setOnClickListener(this);
        ivSetting = (ImageView) llBottom.findViewById(R.id.ivSetting);
        ivSetting.setOnClickListener(this);
        tvAmbulanceNo = (TextView) llBottom.findViewById(R.id.tvAmbulanceNo);
        tvMapNo = (TextView) llBottom.findViewById(R.id.tvMapNo);
        tvNotificationNo = (TextView) llBottom.findViewById(R.id.tvNotificationNo);
        tvSettingNo = (TextView) llBottom.findViewById(R.id.tvSettingNo);
        mapFragment = new MapFragment();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getSupportFragmentManager().beginTransaction().add(R.id.contentFrame, mapFragment, Const.FRAGMENT_MAP).commit();
        IntentFilter filter = new IntentFilter(Const.INTENT_CRASH_NOTIFICATION);
        LocalBroadcastManager.getInstance(this).registerReceiver(new CrashNotificationReceiver(), filter);

//        if(getIntent() != null){
//            getIntent().getIntExtra("ambulanceId", 0);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(findViewById(R.id.contentFrame1) == null) {
            Const.isMobile = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        else{
            Const.isMobile = false;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showLocationOffDialog();
        } else {
            removeLocationOffDialog();
        }
//        registerReceiver(internetConnectionReceiver, new IntentFilter(
//                "android.net.conn.CONNECTIVITY_CHANGE"));
//        registerReceiver(GpsChangeReceiver, new IntentFilter(
//                LocationManager.PROVIDERS_CHANGED_ACTION));
//        isReceiverRegistered = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivAmbulance:
                ivAmbulance.setAlpha(1.0f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(0.6f);
                loadAmbulanceFragment();
                break;

            case R.id.ivMap:
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(1.0f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(0.6f);
                loadMapFragment();
                break;

            case R.id.ivNotification:
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(1.0f);
                //                isFromBottomBar = true;
                isNotification = true;
                tvNotificationNo.setVisibility(View.GONE);
                Const.CRASH_NOTIFICATION_COUNT = 0;
                loadAmbulanceImagesFragment();
                break;

            case R.id.ivSetting:
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(1.0f);
                ivNotification.setAlpha(0.6f);
                loadSettingFragment();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        clearBackStack();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Exit me", true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Const.PERMISSION_LOCATION:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        mapFragment.setupMap();
                    }
                }
                break;

            case Const.PERMISSION_CALL:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        new AmbulanceImagesFragment().makeACall();
                    }
                }
                break;
        }
    }

    private class CrashNotificationReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            removeNotification();
            tvNotificationNo.setText(String.valueOf(Const.CRASH_NOTIFICATION_COUNT));
            tvNotificationNo.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
//        if(isReceiverRegistered) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(internetConnectionReceiver);
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(GpsChangeReceiver);
//        }
    }

    @Override
    protected void onDestroy() {
//        if(isReceiverRegistered) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(internetConnectionReceiver);
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(GpsChangeReceiver);
//        }
        super.onDestroy();
    }
}
