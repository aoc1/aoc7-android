package com.aoc7.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aoc7.R;
import com.aoc7.utils.Const;

/**
 * Created by mypc on 29/7/16.
 */
public class ImageFragment extends BaseFragment implements View.OnClickListener{
    private ImageView ivDetail;
    private ImageView ivCardio;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        ivDetail  = (ImageView) view.findViewById(R.id.ivDetail);
        if(!Const.isMobile) {
            ivCardio = (ImageView) view.findViewById(R.id.ivCardio);
            ivDetail.setOnClickListener(this);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivDetail:
                ivCardio.setVisibility(View.VISIBLE);
                ivDetail.setVisibility(View.GONE);
        }
    }
}
