package com.aoc7.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.aoc7.R;
import com.aoc7.adapter.AmbulanceImagesAdapter;
import com.aoc7.adapter.StaffListAdapter;
import com.aoc7.models.AmbulanceDetail;
import com.aoc7.models.Staff;
import com.aoc7.parse.HttpRequester;
import com.aoc7.utils.AndyUtils;
import com.aoc7.utils.Const;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class AmbulanceImagesFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback {
    private TextView tvDepartureTime, tvArrivalTime, tvFastTrack, tvAmbName, tvAmbNo, tvAmbType, tvHospital, tvDiseaseType, tvAmbSpeed, tvStatus;
    private AmbulanceDetail ambulanceDetail;
    private ImageView ivAmb1, ivAmb2, ivCall, ivWave;
    private SupportMapFragment ivAmb3;
    private AQuery aQuery;
    private SeekBar pBarAmb;
    private ArrayList<AmbulanceDetail> ambDetailList = new ArrayList<>();
    private ListView lvAmbImages;
    private AmbulanceImagesAdapter ambulanceImagesAdapter;
    private Timer timer;
    private ImageOptions imageOptions;
    private LinearLayout llStaff, llEquipment, llStaffIcon, llEquipmentIcon;
    private boolean isFirstCall = true;
    Marker marker = null;
    private MapFragment fragment;
    private GoogleMap map;
    private float currentZoom = -1;
    private ArrayList<Staff> staffList;
    private Staff staff;
    private double dist;
    private int ambId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        if (activity.isFromBottomBar || activity.isNotification) {
            view = inflater.inflate(R.layout.fragment_ambulance_images, container, false);
            lvAmbImages = (ListView) view.findViewById(R.id.lvAmbImages);
        } else {
            view = inflater.inflate(R.layout.layout_ambulance_images, container, false);
            tvDepartureTime = (TextView) view.findViewById(R.id.tvDepartureTime);
            tvArrivalTime = (TextView)view.findViewById(R.id.tvArrivalTime);
            tvFastTrack = (TextView) view.findViewById(R.id.tvFastTrack);
            tvAmbName = (TextView) view.findViewById(R.id.tvAmbName);
            tvAmbNo = (TextView) view.findViewById(R.id.tvAmbNo);
            tvAmbSpeed = (TextView) view.findViewById(R.id.tvAmbSpeed);
            tvAmbType = (TextView) view.findViewById(R.id.tvAmbType);
            tvHospital = (TextView) view.findViewById(R.id.tvHospital);
            tvDiseaseType = (TextView) view.findViewById(R.id.tvDiseaseType);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            llEquipment = (LinearLayout) view.findViewById(R.id.llEquipment);
            llStaffIcon = (LinearLayout) view.findViewById(R.id.llStaffIcon);
            llEquipmentIcon = (LinearLayout) view.findViewById(R.id.llEquipmentIcon);
            llStaff = (LinearLayout) view.findViewById(R.id.llStaff);
            ivAmb1 = (ImageView) view.findViewById(R.id.ivAmb1);
            ivAmb2 = (ImageView) view.findViewById(R.id.ivAmb2);
            ivAmb3 = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.ivAmb3);
            pBarAmb = (SeekBar) view.findViewById(R.id.pBarAmb);
            ivCall = (ImageView) view.findViewById(R.id.ivCall);
            ivWave = (ImageView) view.findViewById(R.id.ivWave);
            ivCall.setOnClickListener(this);
            ivWave.setOnClickListener(this);
            ivCall.setOnClickListener(this);
            ivWave.setOnClickListener(this);
            pBarAmb.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
            pBarAmb.setThumb(new ColorDrawable(ContextCompat.getColor(activity, R.color.color_green_button)));
            aQuery = new AQuery(activity);
            imageOptions = new ImageOptions();
            imageOptions.fileCache = true;
            imageOptions.memCache = true;
            imageOptions.targetWidth = 200;
        }
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapFragment fragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_MAP);
        if (activity.isAmbMApShown) {
            fragment.llTypeIcon.setVisibility(View.VISIBLE);
            fragment.startGettingAmbulances();
            activity.isAmbMApShown = false;
        }

        IntentFilter filter = new IntentFilter(Const.INTENT_STATUS_CHANGE);
        if(!activity.isNotification){
            ambId = activity.getIntent().getIntExtra("ambulanceId", 0);
            LocalBroadcastManager.getInstance(activity).registerReceiver(new StatusChangeReceiver(), filter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.ivAmbulance.setAlpha(0.6f);
        activity.ivMap.setAlpha(0.6f);
        activity.ivNotification.setAlpha(1.0f);
        activity.ivSetting.setAlpha(0.6f);
        activity.tvAmbulanceNo.setVisibility(View.GONE);
        activity.tvMapNo.setVisibility(View.GONE);
        activity.tvNotificationNo.setVisibility(View.GONE);
        activity.tvSettingNo.setVisibility(View.GONE);

        if (getArguments() == null) {
            Log.i("imgFragment", "onResume if called");
            getNotification();
        } else if (getArguments().getSerializable("ambDetailList") != null) {
            Log.i("imgFragment", "onResume first else if called");
            ambDetailList = (ArrayList<AmbulanceDetail>) getArguments().getSerializable("ambDetailList");
//            if(ambDetailList != null && ambDetailList.size() > 0) {
//                for (int i = 0; i < ambDetailList.size(); i++) {
//                    if (ambDetailList.get(i).getIsOnline().equalsIgnoreCase("yes")) {
//                        ambulanceDetail = ambDetailList.get(i);
//                        i = ambDetailList.size() + 1;
//                    }
//                }
//            }
            ambulanceImagesAdapter = new AmbulanceImagesAdapter(activity, ambDetailList);
            lvAmbImages.setAdapter(ambulanceImagesAdapter);
//            activity.tvNotificationNo.setText(String.valueOf(ambDetailList.size()));
//            activity.tvNotificationNo.setVisibility(View.VISIBLE);
//            activity.tvAmbulanceNo.setVisibility(View.GONE);
//            activity.tvMapNo.setVisibility(View.GONE);
//            activity.tvSettingNo.setVisibility(View.GONE);
        } else if (getArguments().getSerializable("ambulanceDetail") != null) {
            Log.i("imgFragment", "onResume second else if called");
            ambulanceDetail = (AmbulanceDetail) getArguments().getSerializable("ambulanceDetail");
            if (isFirstCall) {
                activity.ivAmbulance.setAlpha(1.0f);
                activity.ivMap.setAlpha(0.6f);
                activity.ivNotification.setAlpha(0.6f);
                activity.ivSetting.setAlpha(0.6f);
                activity.tvAmbulanceNo.setVisibility(View.GONE);
                activity.tvMapNo.setVisibility(View.GONE);
                activity.tvNotificationNo.setVisibility(View.GONE);
                activity.tvSettingNo.setVisibility(View.GONE);
                AndyUtils.showCustomProgressDialog(activity, false);
            }
            timer = new Timer();
            timer.scheduleAtFixedRate(new GetImageTimerTask(), Const.DELAY, Const.TIME_SCHEDULE);
        }
    }

    private void setData() {
        String ETA;
        if (!Const.isMobile) {
            aQuery.id(ivAmb1).image(ambulanceDetail.getImages().get(0), imageOptions);
            aQuery.id(ivAmb2).image(ambulanceDetail.getImages().get(1), imageOptions);
            fragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_MAP);
            fragment.stopGettingAmbulance();
            fragment.ambOnlineList.clear();
            fragment.ambulanceDetailAdapter.notifyDataSetChanged();
            if (isFirstCall) {
                activity.isAmbMApShown = true;
                fragment.llTypeIcon.setVisibility(View.GONE);
                marker = fragment.markerList.get(ambulanceDetail.getAmbulanceId());
                for (int i = 0; i < fragment.markerList.size(); i++) {
                    if (marker != fragment.markerList.get((activity.ambDetailList.get(i)).getAmbulanceId())) {
                        fragment.markerList.get((activity.ambDetailList.get(i)).getAmbulanceId()).setVisible(false);
                    }
                }
                fragment.animateCameraToMarker(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), false);
            } else if (marker != null) {
                Location location = new Location("");
                location.setLatitude(ambulanceDetail.getCurrentLat());
                location.setLongitude(ambulanceDetail.getCurrentLng());
                marker.setPosition(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));
                fragment.animateMarker(marker, new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), location, false);
                fragment.animateCameraToMarker(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), true);
            }
        } else {
            aQuery.id(ivAmb1).image(ambulanceDetail.getImages().get(0), imageOptions);
            aQuery.id(ivAmb2).image(ambulanceDetail.getImages().get(1), imageOptions);
//            aQuery.id(ivAmb3).image(ambulanceDetail.getImages().get(2), imageOptions);
            if(isFirstCall)
                setupMap();
        }

        if (isFirstCall) {
            if (ambulanceDetail.getEquipmentList().size() > 0) {
                llEquipment.setVisibility(View.VISIBLE);
                for (int i = 0; i < ambulanceDetail.getEquipmentList().size(); i++) {
                    LayoutInflater inflater = LayoutInflater.from(activity);
                    View view = inflater.inflate(R.layout.layout_icon_inflater, null);
                    ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                    aQuery.id(ivIcon).image(ambulanceDetail.getEquipmentList().get(i).getEquipmentIcon());
                    if (ivIcon.getParent() != null) {
                        ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                    }
                    llEquipmentIcon.addView(ivIcon);
                }
            }
            if (ambulanceDetail.getDoctor() == 1 || ambulanceDetail.getDriver() == 1 || ambulanceDetail.getNurse() == 1 || ambulanceDetail.getEr() == 1 || ambulanceDetail.getEms() == 1) {
                llStaff.setVisibility(View.VISIBLE);
                LayoutInflater inflater = LayoutInflater.from(activity);
                if (ambulanceDetail.getDoctor() == 1) {
                    View view = inflater.inflate(R.layout.layout_icon_inflater, null);
                    ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                    ivIcon.setImageResource(R.mipmap.doctor);
                    if (ivIcon.getParent() != null) {
                        ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                    }
                    llStaffIcon.addView(ivIcon);
                }
                if (ambulanceDetail.getDriver() == 1) {
                    View view = inflater.inflate(R.layout.layout_icon_inflater, null);
                    ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                    ivIcon.setImageResource(R.mipmap.driver);
                    if (ivIcon.getParent() != null) {
                        ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                    }
                    llStaffIcon.addView(ivIcon);
                }
                if (ambulanceDetail.getNurse() == 1) {
                    View view = inflater.inflate(R.layout.layout_icon_inflater, null);
                    ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                    ivIcon.setImageResource(R.mipmap.nurse);
                    if (ivIcon.getParent() != null) {
                        ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                    }
                    llStaffIcon.addView(ivIcon);
                }
                if (ambulanceDetail.getEms() == 1) {
                    View view = inflater.inflate(R.layout.layout_icon_inflater, null);
                    ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                    ivIcon.setImageResource(R.mipmap.emr);
                    if (ivIcon.getParent() != null) {
                        ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                    }
                    llStaffIcon.addView(ivIcon);
                }
                if (ambulanceDetail.getEr() == 1) {
                    View view = inflater.inflate(R.layout.layout_icon_inflater, null);
                    ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                    ivIcon.setImageResource(R.mipmap.ref_patient);
                    if (ivIcon.getParent() != null) {
                        ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                    }
                    llStaffIcon.addView(ivIcon);
                }
            }
            this.staffList = ambulanceDetail.getStaffList();
            tvAmbName.setText(ambulanceDetail.getName());
            tvAmbNo.setText(ambulanceDetail.getAmbulanceNo());
            tvAmbType.setText(ambulanceDetail.getType());
            if(tvHospital != null) {
                tvHospital.setText(activity.getString(R.string.text_from) + " " + ambulanceDetail.getHospitalList().get(0) + " " + activity.getString(R.string.text_to) + " " + ambulanceDetail.getHospitalList().get(1));

            }
            if(!TextUtils.isEmpty(ambulanceDetail.getFastTrack()))
                tvFastTrack.setText(ambulanceDetail.getFastTrack());
            if (!TextUtils.isEmpty(ambulanceDetail.getDiseaseName())) {
                tvDiseaseType.setText(ambulanceDetail.getDiseaseName());
            } else {
                tvDiseaseType.setText("N/A");
            }
            isFirstCall = false;
        }

        tvDepartureTime.setText(ambulanceDetail.getTrackingTime().substring(10));
        tvArrivalTime.setText(calculateArrivalTime());
        tvStatus.setText(ambulanceDetail.getState());
        dist = Math.sqrt(Math.pow(ambulanceDetail.getCurrentLat() - ambulanceDetail.getDestLat(), 2) + Math.pow(ambulanceDetail.getCurrentLng() - ambulanceDetail.getDestLng(), 2));
        double minute = ((dist * 100) / (ambulanceDetail.getSpeed() * 60 / 1000));
        double speed = ambulanceDetail.getSpeed() * 60 * 60 / 1000;
        Log.i("Minute","----->>>>" + minute);
        if(speed >= 0.00 && speed <= 1.00){
            ETA = getString(R.string.text_infinity);
        }
        else if(minute > 60){
            double hour  = minute / 60;
            int roundHour = (int)(minute / 60);
            int min = (int)((hour - roundHour) * 60);
            ETA = roundHour + " hour " + min + " Min" + String.format(Locale.ENGLISH,"%.2f", ambulanceDetail.getSpeed() * 60 * 60 / 1000) + " km/h ";
        }
        else{
            ETA = String.format(Locale.ENGLISH, "%.2f",minute) + " Min " + String.format(Locale.ENGLISH,"%.2f", ambulanceDetail.getSpeed() * 60 * 60 / 1000) + " km/h ";
        }
        tvAmbSpeed.setText(ETA);

        if ((ambulanceDetail.getSpeed() * 60 * 60 / 1000) > 80 && (ambulanceDetail.getSpeed() * 60 * 60 / 1000) < 120) {
            tvAmbSpeed.setTextColor(ContextCompat.getColor(activity, R.color.color_orange));
        } else if ((ambulanceDetail.getSpeed() * 60 * 60 / 1000) >= 120) {
            tvAmbSpeed.setTextColor(ContextCompat.getColor(activity, R.color.color_red));
        } else {
            tvAmbSpeed.setTextColor(ContextCompat.getColor(activity, R.color.color_green));
        }
        Location location1 = new Location("");
        Location location2 = new Location("");
        location1.setLatitude(ambulanceDetail.getSrcLat());
        location1.setLongitude(ambulanceDetail.getSrcLng());
        location2.setLatitude(ambulanceDetail.getDestLat());
        location2.setLongitude(ambulanceDetail.getDestLng());

        double totalDist = location1.distanceTo(location2);

        location1 = new Location("");
        location2 = new Location("");
        location1.setLatitude(ambulanceDetail.getSrcLat());
        location1.setLongitude(ambulanceDetail.getSrcLng());
        location2.setLatitude(ambulanceDetail.getCurrentLat());
        location2.setLongitude(ambulanceDetail.getCurrentLng());

        double coveredDist = location1.distanceTo(location2);
        pBarAmb.setProgress((int) Math.floor(Math.floor((100 * coveredDist) / totalDist)));
        AndyUtils.removeCustomProgressDialog();
    }

    private String calculateArrivalTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, (int)((dist * 100) / (ambulanceDetail.getSpeed() * 60 / 1000)));
        return sdf.format(calendar.getTime());
    }

    private void getNotification() {
        if (!Const.isMobile) {
            fragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_MAP);
            fragment.stopGettingAmbulance();
        }
        AndyUtils.showCustomProgressDialog(activity, false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.GET_NOTIFICATION);
        new HttpRequester(activity, map, Const.ServiceCode.GET_NOTIFICATION, true, this);
    }

    private void setupMap(){
        if (map == null) {
            AndyUtils.showCustomProgressDialog(activity, false);
            ivAmb3.getMapAsync(this);
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);
        switch (serviceCode) {
            case Const.ServiceCode.GET_NOTIFICATION:
                AndyUtils.removeCustomProgressDialog();
                Log.i("notification", "Response----->>>" + response);
                if (pContent.isSuccess(response)) {
                    ambDetailList.clear();
                    pContent.parseNotificationList(response, ambDetailList);
                    ambulanceImagesAdapter = new AmbulanceImagesAdapter(activity, ambDetailList);
                    lvAmbImages.setAdapter(ambulanceImagesAdapter);
                }
                break;

            case Const.ServiceCode.GET_AMBULANCE_IMAGE:
                if (pContent.isSuccess(response)) {
                    ambulanceDetail = pContent.parseAmbulanceImage(response);
                    if (timer != null)
                        setData();
                }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCall:
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
                if (ambulanceDetail.getStaffList() != null && ambulanceDetail.getStaffList().size() > 0) {
                    showStaffListDialog();
                } else {
                    AndyUtils.showToast(activity.getString(R.string.text_staff_not_available), activity);
                }
                break;

            case R.id.ivWave:
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
                activity.llBottom.setVisibility(View.GONE);
                if (Const.isMobile) {
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new ImageFragment(), Const.FRAGMENT_IMAGE).commit();
                } else {
                    AmbulanceImagesFragment fragment1 = (AmbulanceImagesFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_AMBULANCE_IMAGES);
                    activity.getSupportFragmentManager().beginTransaction().remove(fragment1).commit();
                    activity.frameLayout.setVisibility(View.GONE);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame, new ImageFragment(), Const.FRAGMENT_IMAGE).commit();
                }
                break;
        }
    }

    private void showStaffListDialog() {
        Dialog staffListDialog = new Dialog(activity);
        staffListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        staffListDialog.setContentView(R.layout.dialog_staff_list);
        ListView lvStaffList = (ListView) staffListDialog.findViewById(R.id.lvStaffList);
        final StaffListAdapter staffListAdapter = new StaffListAdapter(activity, this.staffList);
        lvStaffList.setAdapter(staffListAdapter);
        lvStaffList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                staff = staffListAdapter.getItem(position);
                makeACall();
            }
        });
        staffListDialog.show();
    }

    public void makeACall(){
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + staff.getNumber()));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
//                    return;
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, Const.PERMISSION_CALL);
                return;
            }
            else{
                activity.startActivity(intent);
            }
        }
        else{
            activity.startActivity(intent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
            }
        }
        else {
            map = googleMap;
            map.setMyLocationEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if(currentZoom == -1){
                        currentZoom = cameraPosition.zoom;
                    }
                    else if(cameraPosition.zoom != currentZoom){
                        currentZoom = cameraPosition.zoom;
                    }
                }
            });
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
//                   getAmbulanceList();
//                    startGettingAmbulances();
                    startUpdatingAmbulanceOnMap();
                }
            });
        }
    }

    private void startUpdatingAmbulanceOnMap(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new UpdateAmbulanceTimerTask(), Const.DELAY, Const.TIME_SCHEDULE);
    }

    private class UpdateAmbulanceTimerTask extends TimerTask{
        @Override
        public void run() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(marker == null){
                        MarkerOptions markerOptions = activity.createMarker(ambulanceDetail);
                        marker = map.addMarker(markerOptions);
//                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), 15));
                        animateCameraToMarker(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), true);
                    }
                    else{
                        marker.setPosition(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));
                        Location location = new Location("");
                        location.setLatitude(ambulanceDetail.getCurrentLat());
                        location.setLongitude(ambulanceDetail.getCurrentLng());
                        animateMarker(marker, new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), location, false);
                        animateCameraToMarker(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), true);
                    }
                }
            });
        }
    }

    public void animateCameraToMarker(LatLng latLng, boolean isAnimate) {
        Log.i("mapFragment","animateCameraToMarker called");
        try {
            CameraUpdate cameraUpdate;
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);

            if (map != null) {
                Log.i("mapFragment","animateCameraToMarker if called");
                if (isAnimate)
                    map.animateCamera(cameraUpdate);
                else
                    map.moveCamera(cameraUpdate);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void animateMarker(final Marker marker, final LatLng toPosition,
                                 final Location toLocation, final boolean hideMarker) {
        if (map == null || !this.isVisible()) {
            return;
        }
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection projection = map.getProjection();
        Point startPoint = projection.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                float rotation = (float) (t * toLocation.getBearing() + (1 - t)
                        * startRotation);
                if (rotation != 0) {
                    marker.setRotation(rotation);
                }
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private class GetImageTimerTask extends TimerTask{
        @Override
        public void run() {
            getAmbulanceImages(ambulanceDetail.getAmbulanceId());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    private void getAmbulanceImages(int ambulanceId){

        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.GET_AMBULANCE_IMAGE + Const.Params.AMBULANCE_ID + "=" + ambulanceId);
        new HttpRequester(activity, map, Const.ServiceCode.GET_AMBULANCE_IMAGE, true, AmbulanceImagesFragment.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Const.PERMISSION_CALL:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        makeACall();
                    }
                }
                break;
        }
    }

    private class StatusChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            activity.removeNotification();
            getAmbulanceImages(ambId);
        }
    }
}
