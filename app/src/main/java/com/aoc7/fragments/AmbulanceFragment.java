package com.aoc7.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aoc7.R;
import com.aoc7.adapter.AmbulanceListAdapter;
import com.aoc7.models.AmbulanceDetail;
import com.aoc7.parse.HttpRequester;
import com.aoc7.utils.AndyUtils;
import com.aoc7.utils.Const;
import com.hb.views.PinnedSectionListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.TreeSet;

public class AmbulanceFragment extends BaseFragment implements View.OnClickListener{
    private LinearLayout llSearchBar;
    private EditText etSearch;
    private PinnedSectionListView lvAmbulanceList;
    private ArrayList<AmbulanceDetail> ambDetailList = new ArrayList<>(), ambListOrg = new ArrayList<>();
    private AmbulanceListAdapter ambulanceListAdapter;
    private boolean isFilterApplied = false;
    private TextView tvOn, tvStb, tvEmsAmb, tvRefAmb, tvAllAmb;
    private TreeSet<Integer> mSeparatorSet = new TreeSet<>();
    private ArrayList<String> hospitalList = new ArrayList<>();
    private MapFragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ambulance_list, container, false);
        llSearchBar = (LinearLayout) view.findViewById(R.id.llSearchBar);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        lvAmbulanceList = (PinnedSectionListView) view.findViewById(R.id.lvAmbulanceList);
        lvAmbulanceList.setDivider(new ColorDrawable(ContextCompat.getColor(activity,R.color.color_light_grey)));
        tvOn = (TextView) view.findViewById(R.id.tvOn);
        tvOn.setOnClickListener(this);
        tvStb = (TextView) view.findViewById(R.id.tvStb);
        tvStb.setOnClickListener(this);
        tvAllAmb = (TextView) view.findViewById(R.id.tvAllAmb);
        tvAllAmb.setOnClickListener(this);
        tvEmsAmb = (TextView) view.findViewById(R.id.tvEmsAmb);
        tvEmsAmb.setOnClickListener(this);
        tvRefAmb = (TextView) view.findViewById(R.id.tvRefAmb);
        tvRefAmb.setOnClickListener(this);
        llSearchBar.setOnClickListener(this);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager = (InputMethodManager)activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    searchAmbulances(etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
        if(!Const.isMobile){
            fragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_MAP);
            fragment.stopGettingAmbulance();
        }
        getAmbulanceList();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(activity.isAmbMApShown){
            MapFragment fragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_MAP);
            fragment.llTypeIcon.setVisibility(View.VISIBLE);
            fragment.startGettingAmbulances();
            activity.isAmbMApShown = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.ivAmbulance.setAlpha(1.0f);
        activity.ivMap.setAlpha(0.6f);
        activity.ivNotification.setAlpha(0.6f);
        activity.ivSetting.setAlpha(0.6f);
        activity.tvAmbulanceNo.setVisibility(View.VISIBLE);
        activity.tvMapNo.setVisibility(View.GONE);
        activity.tvNotificationNo.setVisibility(View.GONE);
        activity.tvSettingNo.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llSearchBar:
                break;

            case R.id.tvOn:
//                filterList.clear();
//                for(int i=0; i<ambDetailList.size(); i++){
//                    if(ambDetailList.get(i).getType().equalsIgnoreCase("on"))
//                }
                tvOn.setAlpha(Float.parseFloat("1"));
                tvStb.setAlpha(Float.parseFloat("0.6"));
                tvEmsAmb.setAlpha(Float.parseFloat("0.6"));
                tvRefAmb.setAlpha(Float.parseFloat("0.6"));
                tvAllAmb.setAlpha(Float.parseFloat("0.6"));

                isFilterApplied = true;
                break;

            case R.id.tvStb:
                tvOn.setAlpha(Float.parseFloat("0.6"));
                tvStb.setAlpha(Float.parseFloat("1"));
                tvEmsAmb.setAlpha(Float.parseFloat("0.6"));
                tvRefAmb.setAlpha(Float.parseFloat("0.6"));
                tvAllAmb.setAlpha(Float.parseFloat("0.6"));
                isFilterApplied = true;
                ambulanceListAdapter.isFilterByName = false;
                ambulanceListAdapter.getFilter().filter(getString(R.string.text_stb));
                break;

            case R.id.tvAllAmb:
                if(isFilterApplied) {
                    tvOn.setAlpha(Float.parseFloat("0.6"));
                    tvStb.setAlpha(Float.parseFloat("0.6"));
                    tvEmsAmb.setAlpha(Float.parseFloat("0.6"));
                    tvRefAmb.setAlpha(Float.parseFloat("0.6"));
                    tvAllAmb.setAlpha(Float.parseFloat("1"));
//
//                    filterList.clear();
//                    for (int i = 0; i < ambDetailList.size(); i++) {
//                        filterList.add(ambDetailList.get(i));
//                    }
//                    ambulanceListAdapter.notifyDataSetChanged();

                    ambulanceListAdapter.isFilterByName = false;
                    ambulanceListAdapter.getFilter().filter(Const.TYPE_ALL);
                    isFilterApplied = false;
                }
                break;

            case R.id.tvRefAmb:
                tvOn.setAlpha(Float.parseFloat("0.6"));
                tvStb.setAlpha(Float.parseFloat("0.6"));
                tvEmsAmb.setAlpha(Float.parseFloat("0.6"));
                tvRefAmb.setAlpha(Float.parseFloat("1"));
                tvAllAmb.setAlpha(Float.parseFloat("0.6"));

                isFilterApplied = true;
//                filterList.clear();
//                for(int i=0; i<ambDetailList.size(); i++){
//                    if(ambDetailList.get(i).getType().equalsIgnoreCase("ref")){
//                        filterList.add(ambDetailList.get(i));
//                    }
//                }
//                ambulanceListAdapter.notifyDataSetChanged();
                ambulanceListAdapter.isFilterByName = false;
                ambulanceListAdapter.getFilter().filter(Const.TYPE_REF);
                break;

            case R.id.tvEmsAmb:
                tvOn.setAlpha(Float.parseFloat("0.6"));
                tvStb.setAlpha(Float.parseFloat("0.6"));
                tvEmsAmb.setAlpha(Float.parseFloat("1"));
                tvRefAmb.setAlpha(Float.parseFloat("0.6"));
                tvAllAmb.setAlpha(Float.parseFloat("0.6"));

//                filterList.clear();
//                for(int i=0; i<ambDetailList.size(); i++){
//                    if(ambDetailList.get(i).getType().equalsIgnoreCase("ems")){
//                        filterList.add(ambDetailList.get(i));
//                    }
//                }
//                ambulanceListAdapter.notifyDataSetChanged();
                isFilterApplied = true;
                ambulanceListAdapter.isFilterByName = false;
                ambulanceListAdapter.getFilter().filter(Const.TYPE_EMS);
                break;
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);
        switch (serviceCode){
            case Const.ServiceCode.GET_AMBULANCES:
                Log.d("GetAmbulance","Response------>>>" + response);
                if(pContent.isSuccess(response)){
                    ambListOrg.clear();
                    mSeparatorSet.clear();
                    hospitalList.clear();
                    pContent.parseAmbulanceList(response, ambDetailList);
                    if(ambDetailList.size() > 0) {
                        String ETA;
                        for(int i=0; i<ambDetailList.size(); i++){
                            if(ambDetailList.get(i).getIsOnline().equalsIgnoreCase("yes")){
                                double dist = Math.sqrt(Math.pow(ambDetailList.get(i).getCurrentLat() - ambDetailList.get(i).getDestLat(), 2) + Math.pow(ambDetailList.get(i).getCurrentLng() - ambDetailList.get(i).getDestLng(), 2));
                                double minute = ((dist * 100) / (ambDetailList.get(i).getSpeed() * 60 / 1000));
                                double speed = ambDetailList.get(i).getSpeed() * 60 * 60 / 1000;
                                Log.i("Minute","----->>>>" + minute);
                                if(speed >= 0.00 && speed <= 1.00){
                                    ETA = activity.getString(R.string.text_infinity);
                                }
                                else if(minute > 60){
                                    double hour  = minute / 60;
                                    int roundHour = (int)(minute / 60);
                                    int min = (int)((hour - roundHour) * 60);
                                    ETA = roundHour + " hour " + min + " Min" + String.format(Locale.ENGLISH,"%.2f", ambDetailList.get(i).getSpeed() * 60 * 60 / 1000) + " km/h ";
                                }
                                else{
                                    ETA = String.format(Locale.ENGLISH, "%.2f",minute) + " Min " + String.format(Locale.ENGLISH,"%.2f", ambDetailList.get(i).getSpeed() * 60 * 60 / 1000) + " km/h ";
                                }
                                Log.i("amdList","amb name " + ambDetailList.get(i).getName() + " ETA " + ETA);
                                ambDetailList.get(i).setEta(ETA);
                            }
                        }
//                        filterList.clear();
//                        for(int i=0; i<ambDetailList.size(); i++){
//                            filterList.add(ambDetailList.get(i));
//                        }
                        HashSet<String> listToSet = new HashSet<>();
                        for(int i=0; i<ambDetailList.size(); i++){
                            if(listToSet.add(ambDetailList.get(i).getHospitalName())){
                                hospitalList.add(ambDetailList.get(i).getHospitalName());
                            }
                        }

                        for(int i=0; i<hospitalList.size(); i++){
                            AmbulanceDetail ambulanceDetail = new AmbulanceDetail();
                            ambulanceDetail.setHospitalName(hospitalList.get(i));

                            ambListOrg.add(ambulanceDetail);
                            mSeparatorSet.add(ambListOrg.size() - 1);
                            for(int j=0; j<ambDetailList.size(); j++){
                                if(ambulanceDetail.getHospitalName().equals(ambDetailList.get(j).getHospitalName())){
                                    ambListOrg.add(ambDetailList.get(j));
                                }
                            }
                        }
                        ambulanceListAdapter = new AmbulanceListAdapter(activity, mSeparatorSet, ambListOrg);
                        lvAmbulanceList.setAdapter(ambulanceListAdapter);
                        activity.tvAmbulanceNo.setText(String.valueOf(ambDetailList.size()));
                        activity.tvAmbulanceNo.setVisibility(View.VISIBLE);
                        activity.tvNotificationNo.setVisibility(View.GONE);
                        activity.tvMapNo.setVisibility(View.GONE);
                        activity.tvSettingNo.setVisibility(View.GONE);
                        AndyUtils.removeCustomProgressDialog();
                    }
                }
                break;
        }
    }

    private void getAmbulanceList(){
        AndyUtils.showCustomProgressDialog(activity, false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.GET_AMBULANCES);
        new HttpRequester(activity, map, Const.ServiceCode.GET_AMBULANCES, true, this);
    }

    private void searchAmbulances(String keyWord){
        ambulanceListAdapter.isFilterByName = true;
        ambulanceListAdapter.getFilter().filter(keyWord);
    }
}
