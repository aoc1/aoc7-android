package com.aoc7.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.aoc7.BaseActivity;
import com.aoc7.parse.AsyncTaskCompleteListener;
import com.aoc7.parse.ParseContent;
import com.aoc7.utils.PreferenceHelper;

public class BaseFragment extends Fragment implements AsyncTaskCompleteListener{

    public BaseActivity activity;
    public PreferenceHelper pHelper;
    public ParseContent pContent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if(getActivity() instanceof MapActivity)
            activity = (BaseActivity) getActivity();

        pHelper = new PreferenceHelper(activity);
        pContent = new ParseContent(activity);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

    }
}
