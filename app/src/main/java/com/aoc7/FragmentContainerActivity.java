package com.aoc7;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aoc7.fragments.AmbulanceCrashLocationFragment;
import com.aoc7.fragments.AmbulanceFragment;
import com.aoc7.fragments.AmbulanceImagesFragment;
import com.aoc7.fragments.ProfileFragment;
import com.aoc7.fragments.SettingFragment;
import com.aoc7.models.AmbulanceDetail;
import com.aoc7.utils.Const;

public class FragmentContainerActivity extends BaseActivity {

    public String fragmentTag, mFragmentTag;
    private int mFragmentId = 0;
    private boolean isProfileAdded = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        findViewById(R.id.frameLayout);
        llBottom = (LinearLayout) findViewById(R.id.llBottom);
        ivAmbulance = (ImageView) llBottom.findViewById(R.id.ivAmbulance);
        ivAmbulance.setOnClickListener(this);
        ivMap = (ImageView) llBottom.findViewById(R.id.ivMap);
        ivMap.setOnClickListener(this);
        ivNotification = (ImageView) llBottom.findViewById(R.id.ivNotification);
        ivNotification.setOnClickListener(this);
        ivSetting = (ImageView) llBottom.findViewById(R.id.ivSetting);
        ivSetting.setOnClickListener(this);
        tvAmbulanceNo = (TextView) llBottom.findViewById(R.id.tvAmbulanceNo);
        tvMapNo = (TextView) llBottom.findViewById(R.id.tvMapNo);
        tvNotificationNo = (TextView) llBottom.findViewById(R.id.tvNotificationNo);
        tvSettingNo = (TextView) llBottom.findViewById(R.id.tvSettingNo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Const.isMobile = true;
        if(!isProfileAdded) {
            fragmentTag = getIntent().getStringExtra("fragmentTag");
            Log.i("fragmentTag", "---->>>" + fragmentTag);
            if (fragmentTag.equalsIgnoreCase(Const.FRAGMENT_AMBULANCE_IMAGES)) {
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(1.0f);
                AmbulanceImagesFragment ambulanceImagesFragment = new AmbulanceImagesFragment();
                if (getIntent().getBundleExtra("bundle") == null) {
                    isNotification = true;
                } else {
                    ambulanceImagesFragment.setArguments(getIntent().getBundleExtra("bundle"));
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, ambulanceImagesFragment, Const.FRAGMENT_AMBULANCE_IMAGES).commitAllowingStateLoss();
            } else if (fragmentTag.equalsIgnoreCase(Const.FRAGMENT_AMBULANCE)) {
                ivAmbulance.setAlpha(1.0f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(0.6f);
                getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, new AmbulanceFragment(), Const.FRAGMENT_AMBULANCE).commitAllowingStateLoss();
            } else if (fragmentTag.equalsIgnoreCase(Const.FRAGMENT_SETTING)) {
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(1.0f);
                ivNotification.setAlpha(0.6f);
                getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, new SettingFragment(), Const.FRAGMENT_SETTING).commitAllowingStateLoss();
            }

//        else if(fragmentTag.equalsIgnoreCase(Const.FRAGMENT_AMBULANCE_CRASH_LOCATION)){
//            ivAmbulance.setAlpha(0.6f);
//            ivMap.setAlpha(0.6f);
//            ivSetting.setAlpha(0.6f);
//            ivNotification.setAlpha(1.0f);
//            AmbulanceCrashLocationFragment crashLocationFragment = new AmbulanceCrashLocationFragment();
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("ambulanceDetail", ambulanceDetail);
//            crashLocationFragment.setArguments(bundle);
//            getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, new SettingFragment(), Const.FRAGMENT_AMBULANCE_CRASH_LOCATION).commit();
//        }
        }
    }

    public void loadAmbulanceCrashLocationFragment(AmbulanceDetail ambulanceDetail){
        fragmentTag = Const.FRAGMENT_AMBULANCE_CRASH_LOCATION;
        ivAmbulance.setAlpha(0.6f);
        ivMap.setAlpha(0.6f);
        ivSetting.setAlpha(0.6f);
        ivNotification.setAlpha(1.0f);
        AmbulanceCrashLocationFragment crashLocationFragment = new AmbulanceCrashLocationFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("ambulanceDetail", ambulanceDetail);
        crashLocationFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, crashLocationFragment, Const.FRAGMENT_AMBULANCE_CRASH_LOCATION).commitAllowingStateLoss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivAmbulance:
                ivAmbulance.setAlpha(1.0f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(0.6f);
                loadAmbulanceFragment();
                break;

            case R.id.ivMap:
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(1.0f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(0.6f);
                loadMapFragment();
                break;

            case R.id.ivNotification:
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(0.6f);
                ivNotification.setAlpha(1.0f);
                //                isFromBottomBar = true;
                isNotification = true;
                tvNotificationNo.setVisibility(View.GONE);
                Const.CRASH_NOTIFICATION_COUNT = 0;
                loadAmbulanceImagesFragment();
                break;

            case R.id.ivSetting:
                ivAmbulance.setAlpha(0.6f);
                ivMap.setAlpha(0.6f);
                ivSetting.setAlpha(1.0f);
                ivNotification.setAlpha(0.6f);
                loadSettingFragment();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Const.PERMISSION_LOCATION:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        new AmbulanceCrashLocationFragment().setupMap();
                    }
                }
                break;

            case Const.PERMISSION_GALLLERY:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        new ProfileFragment().choosePhotoFromGallary();
                    }
                }
                break;

            case Const.PERMISSION_CAMERA:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        new ProfileFragment().takePhotoFromCamera();
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MapActivity.class));
        this.finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = null;
        Log.i("FragActivity","onActivityResult called");
        if (mFragmentTag != null && !mFragmentTag.equalsIgnoreCase("")) {
            fragment = getSupportFragmentManager().findFragmentByTag(mFragmentTag);
        }

        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        Log.i("FragActivity","Fragment-->>>" + fragment.getTag());
    }

    public void startActivityForResult(Intent intent, int requestCode,String fragTag) {
        Log.i("FragActivity","startActivityFor Result called");
        isProfileAdded = true;
        mFragmentTag = fragTag;
        super.startActivityForResult(intent, requestCode);
    }
}
